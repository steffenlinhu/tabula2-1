#!/usr/bin/python3
# -*- coding: utf-8 -*-

########################################################################
# infobox liefert zusätzliche Infoseiten bei leerem Platz
########################################################################

# Batteries
import os.path
import random
import time
# TI
import ti

INFO_TXT='''
    <div class="infos" style="text-align:center;">
    <img src="/{filename}{zufallsparameter}" style="max-width: 100%; height: auto; width: auto\9; " />
    {info_by}
    </div>
'''
INFO_TXT_LEHRER='''
    <div class="infos" style="text-align:center;background:blue">
    <img src="/{filename}{zufallsparameter}" style="max-width: 100%; height: auto; width: auto\9; " />
    {info_by}
    </div>
'''
INFO_TXT_MINI='''
    <div class="infos" style="text-align:center;">
    <img src="/{filename}{zufallsparameter}" style="max-width: 50%; height: auto; width: auto\9; " />
    {info_by}
    </div>
'''
INFO_TXT2='''
    <div class="content"><h2> {title}</h2>
    <iframe src="{infourl}" width="99%" height="100px" name="info_in_messages" frameborder="0" style="overflow:hidden">
        Your Browser cannot show iframes
    </iframe>
    </div>
'''

def get_info(ti_ctx,infourl="",title="Info",mini=False):
    thenames=['0.png','1.png','2.png','3.png','4.png','5.png']+['0.gif']*3+['1.gif']*3+['2.gif']*3
    if infourl=="":
        laup=int(ti_ctx.get_ti_lastupload())
        zufallsparameter='' if int(time.time())-laup>3600 else '?a={}'.format(laup)
        ## Logo, ggf. wieder überschrieben
        pic2show='/local/logo.png' # Logo der Schule
        is_info2show=True
        if not os.path.isfile(ti.get_ti_dynpath()+pic2show):
            if not ti_ctx.cnf.get_db_config_bool('Flag_HideTI',False):
                ## TI-Logo, ggf. w. ü
                pic2show='/static/logo.png' # TI-Logo
            else:
                pic2show=''
                is_info2show=False
        info_by=""
        if ti_ctx.req.client.ist_lehrer() and os.path.isfile(ti.get_ti_basepath()+'www/dyn/lehrer.gif'):
            pic2show='dyn/lehrer.gif'
            is_info2show=True
            info_by=ti_ctx.cnf.get_db_config('info_by_lehrer.gif',"")
            if info_by:
                info_by='<br><div style="text-align:right;font-size:80%">'+_('Infoseite hochgeladen von')+' '+info_by+'</div>'
            return is_info2show, INFO_TXT_LEHRER.format(     filename=pic2show, zufallsparameter=zufallsparameter,info_by=info_by)

        else: 
            ## bewusst eine Liste
            bild_nr=list(range(len(thenames)))
            random.shuffle(bild_nr)
            ## alle Bilder-Filenamen abklappern
            for i in bild_nr:
                filename='info'+thenames[i]
                if os.path.isfile(ti.get_ti_dynpath()+filename): # filesystem path
                    ## Ha, ein Bild gefunden - go!
                    pic2show='dyn/'+filename # web path
                    is_info2show=True
                    info_by=ti_ctx.cnf.get_db_config('info_by_'+filename,"")
                    break
        if info_by:
            info_by='<br><div style="text-align:right;font-size:80%">'+_('Infoseite hochgeladen von')+' '+info_by+'</div>'
        if is_info2show:
            if mini: return is_info2show, INFO_TXT_MINI.format(filename=pic2show, zufallsparameter=zufallsparameter,info_by=info_by)
            else:    return is_info2show, INFO_TXT.format(     filename=pic2show, zufallsparameter=zufallsparameter,info_by=info_by)
        else:
            return False,''
    else:
        return True, INFO_TXT2.format(infourl=infourl,title=title)

def selbsttest(ti_ctx):
    ti_ctx.print_html_head("infobox.py SELFTEST")
    i,t=get_info(ti_ctx)
    if i:
        ti.prt(t)
    else:
        ti.prt("<h3>{}</h3>".format(_('Keine Infoseiten anzuzeigen')))
    ti_ctx.print_html_tail()

if __name__ == '__main__':
    ti_ctx=ti.master_init()
    selbsttest(ti_ctx)
