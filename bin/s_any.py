#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_any.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_any.py erzeugt den HTML-Code, um von s_analyzer erzeugte Datei(en) 
    anzuzeigen und die Verwaltungsinformationen an frames.py geben zu können.
________________________________________________________________________
'''

# Batteries
import os
import cgitb; cgitb.enable()
# TI
import ti
import ti_lib
#import s_selection

#class S_Interface(s_selection.ScheduleInterface):
class S_Interface():
    left=-1     # -1: nicht gesetzt, 0...9 vplannummer
    right=-1    # -1: nicht gesetzt, 0...9 vplannummer
    zweitage=False
    cm=0

    schedule_c3_body=''
    clientgruppennummer=0
    vplanliste=[]
    
    def __init__(self,ti_ctx):
        self.ti_ctx=ti_ctx
        
    def analyse_schedule(self,phase,allphases,hochkant=False):
        # Merke: ab nov 2015 ist phase=0 der Aushang, Phase=1 ist erster Plan usw.
        #        self.left ist phase-1, also korrekter Index in vplanliste
        # Ab Feb 2019 nicht mehr minphase & Co sondern Liste allphases
        # Ab April 2019 wird hochkant berücksichtigt
        
        kein_zweitplan=self.ti_ctx.cnf.get_db_config_bool("flag_s_any_kein_zweitplan",False) and self.ti_ctx.cnf.get_db_config_bool("Flag_ShowInfos",False)
        self.debug("s_any sagt: Analyse beginnt ab phase {} für {}".format(phase,allphases))
        self.clientgruppennummer=self.ti_ctx.get_client_max_groups_number()
        self.debug('s_any sagt: clientgruppennummer',str(self.clientgruppennummer))
        #vorläufige Berechnung von nextphase
        nextphase=0
        if len(allphases)>1:
            nextphase=allphases[1] # da das bei Fehlern verwendete phase 0 das nullte sein muss
            self.debug('s_any vorläufiges nextphase bei Abbruch',nextphase)
        self.vplanliste,debuginfo=self.ti_ctx.cnf.get_s_any_vplan_liste(self.clientgruppennummer)
        vplanlen=len(self.vplanliste)
        self.debug('get_s_any_vplan_liste sagt: {} Länge: {}'.format(debuginfo,vplanlen))
        if vplanlen<phase: # für die angegebene phase (>0) gibt es keinen Plan (mehr)
            if vplanlen>0: # wenigstens gibt es überhaupt einen Plan
                self.debug('s_any sagt: Kein Plan für gewünschte Phase')
                return 0, nextphase, 0  # Es gibt Pläne, aber nicht für diese Phase, also phase 0 und evtl. taugt nextphase 1
            else:
                self.debug('s_any sagt: Kein Plan verfügbar')
                return -1, 0, 0 # Info, dass es überhaupt keine Pläne anzuzeigen gibt
            
        if not phase:
            self.left,self.right,self.cm=-1,-1,0
            self.debug('s_any sagt: Es soll der Aushang gezeigt werden')
            return 0, nextphase, self.cm

        # hier kommen wir an, wenn es einen passenden Plan gibt
        # gibt es zwei Tage zum nebeneinander darstellen?
        # links wird sowieso vplanliste[phase-1] dargestellt
        self.left=phase-1
        if (not hochkant) and len(allphases)>allphases.index(phase)+1:
            self.right=allphases[allphases.index(phase)+1]-1
        else:
            self.right=-1
        if not kein_zweitplan and \
           phase<vplanlen and \
           self.vplanliste[self.left ].spalten==1 and \
           self.vplanliste[self.right].spalten==1 and \
           self.right>0:
            self.cm=2
            self.zweitage=True
            self.debug("s_any sagt: Cool, zwei Tage nebeneinander:",[self.left,self.right])
            nextphase=self.right+2 if self.right+2<=vplanlen else 0
            return phase,nextphase, self.cm
        
        #Also gibt es doch nur noch den "phase" anzuzeigen
        if self.vplanliste[phase-1].spalten>=2: # aber wie breit?
            self.cm=2
        else:
            self.cm=1
        self.debug("s_any sagt: nur ein Vertretungsplan mit spaltenzahl",self.cm)
        nextphase=self.right+1 if self.right+1<=vplanlen else 0
        self.right=-1
        return phase,nextphase, self.cm

    def compose_schedule(self,phase, allphases, extrastyle,sc_strg,hochkant):
        if phase!=self.left+1:
            self.ti_ctx.res.debug('Nachträglich geänderte Phase')
            self.analyse_schedule(phase,allphases,hochkant)
        query=''
        if self.ti_ctx.cnf.get_db_config_bool('flag_s_any_schnellscrollen',False):
            query+='QuickScroll=1;'
        if query:
            query='?'+query
        # Die folgende Einbindung der externen Quelle wird vorsichtshalber mit einem Standard-Style versehen. 
        # In der HTML-Datei können natürlich beliebige Styles definiert werden.
        #columnmode=1 # nur linke Hälfte
        #columnmode=2 # für ganze Seite -> dann mit dem alternativen schedule_body
        columnmode=self.cm
        if columnmode==2:
            self.debug("s_any sagt: doppelspaltiger Screen")
            if self.vplanliste[self.left].spalten>=2:
                self.debug("s_any sagt: doppelspaltiger Einzelplan mit Breite",self.vplanliste[self.left].weite)
                if self.vplanliste[self.left].weite>1920:  #FIXME Zahlenwert  # weite impliziert Bildformat
                    x_breite=self.ti_ctx.cnf.get_db_config_int("s_any_pdf_hoch_x_breite",640)
                    extrastyle,timeout=berechne_animation(self.ti_ctx.cnf,x_breite,self.vplanliste[self.left].weite)
                    schedule_body='''
                        <div id="column1_1" class="scroller">
                            <img src="{url}">
                        </div>
                '''.format(url=self.vplanliste[self.left].url+query)
                else:
                    schedule_body='''
                        <div id=column1_1>
                            <iframe class="s_h_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
                        </div>
            '''.format(url=self.vplanliste[self.left].url+query)
            else:
                schedule_body='''
                <div id=column1_2>
                    <iframe class="s_h_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
                </div>
                <div id=column2_2>
                    <iframe class="s_h_iframe" src="{url2}" id="s_html2" frameborder="0">Browser veraltet?</iframe>
                </div>
                '''.format(url=self.vplanliste[self.left].url+query,url2=self.vplanliste[self.right].url+query)
        elif columnmode==1:
            if hochkant:
                schedule_body='''
                    <div id="column1_1" class="scroller">
                        <iframe class="s_h_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
                    </div>
                '''.format(url=self.vplanliste[self.left].url+query) 
            else:
                schedule_body='''
                    <div id=column1_2>
                        <iframe class="s_h_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
                    </div>
                '''.format(url=self.vplanliste[self.left].url+query)
        else:
            schedule_body=""
        
        timeout=10
        if self.left>=0:
            timeout=self.vplanliste[self.left].dauer 
            if self.right>=0:
                timeout+=self.vplanliste[self.right].dauer

        navstrg=''  
        first=True
        for i in range(len(self.vplanliste)): # i durchläuft 0...len-1, zugehörige phasen sind von 1...len
            if first:
                first=False
            else:
                navstrg+= '<span class="main_navigation_selected">+</span>' if i==phase and self.zweitage else "|" 
            
            if phase>0 and ( i==self.left or (i==self.right and self.zweitage) ):
                nfg=' class="main_navigation_selected" '
            elif not (i+1 in allphases): # i+1>allphases[-1]:
                nfg=' class="main_navigation_hide" '
            else:
                nfg=''
            navstrg+= '<a href="/cgi-bin/frames.py?phase={p};longdelay=1{s}">&nbsp;<span {n}>{p}</span>&nbsp;</a>'.format(p=i+1,s=sc_strg,n=nfg)
        if first:
            navstrg=_('keine Pläne')
        return schedule_body, extrastyle, columnmode, navstrg,int(timeout)
    ## End of compose_schedule
 
    def debug(self,*args):
        self.ti_ctx.res.debug(*args,use_caller_caller=True)
 

   
def berechne_animation(ti_cnf,breite,gesamtbreite):
    anzahl=(gesamtbreite+10)//breite-1 # bei 3 seiten werden 2 weggescrollt, so dass noch 1 da steht
    if breite<900: anzahl-=1 # lass 2 seiten stehen, wenn schmale Seiten
    delay=ti_cnf.get_db_config_int('s_any_pdf_seitendauer',10)
    gesamtsekunden=delay*anzahl+anzahl+2 # da immer in einer sekunde gescrollt wird und am Ende zwei Sekunden zurück gescrollt wird
    einesekundeinprozent=100.0/gesamtsekunden
    keyframes=['\n']
    t='\t\t'
    for schritt in range(anzahl+1):
        keyframes.append(t+str(int( (100.0-2*einesekundeinprozent)/(anzahl+1)* schritt                        ))+'% { margin-left:-'+str(schritt*breite)+'px;}\n')
        keyframes.append(t+str(int( (100.0-2*einesekundeinprozent)/(anzahl+1)*(schritt+1)-einesekundeinprozent))+'% { margin-left:-'+str(schritt*breite)+'px;}\n')
    keyframes.append(t+'100%  {   margin-left:0px;} \n')    
    return '''\t<style type="text/css">
        div.scroller img {{
            margin-left:0px;
            -webkit-animation: schieben {dauer}s ease 5s infinite;
                    animation: schieben {dauer}s ease 5s infinite;
        }}
        @-webkit-keyframes schieben {{       {keyframes}    }}
                @keyframes schieben {{       {keyframes}    }}
    
        div.scroller {{
           width:100%;
           height:1040px;
           overflow:hidden;
        }}
    </style>\n'''.format(dauer=str(gesamtsekunden),keyframes="\t".join(keyframes)),gesamtsekunden+5

   
       
if __name__ == '__main__':
    ti_ctx=ti.master_init()
    ti_ctx.res.debug("schedule fuer any startet")
    #schedule_body,dummy1,dummy2,dummy3,dummy4=my_s_interface.compose_schedule(0,[0,1,2,3,4,5,6,7,8,9],"","",False)
    ti_ctx.print_html_head()
    ti.prt("Kein direkter Aufruf vorgesehen")
    ti_ctx.print_html_tail()
    
        

    
