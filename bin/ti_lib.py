#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Batteries (included)
import sys, traceback
import ast, codecs
import os.path, pwd
import sqlite3, time
import socket

import gettext

#TI-Import erst unten nachdem os-Funktionen gelaufen sind!

# Mutter der Porzelankiste
panic_template='''Content-Type: text/html\n\n<!DOCTYPE html>\n<head>
    <style>body, h3, p, pre{{ background:#005;color:#220}} pre {{color:grey}} h3,p {{color:#FF0}}</style>\n</head>
<body>\n\t<br><h3>Fatal Error:</h3>\n<p>{}</p><pre>\n{}</pre>\n'''

def panic(text,ausnahme,details=''):
    ausgabe=panic_template.format(text,str(ausnahme))
    exinfo0=str(sys.exc_info()[0]).replace('<','&lt;').replace('>','&gt;')
    ausgabe+='<pre>{}</pre>\n<pre>{}</pre>\n'.format(exinfo0,details)
    tb=(traceback.format_exc()+'\n ').replace('<','&lt;').replace('>','&gt;').split("\n",1)
    ausgabe+='<h3>{}</h3><br>\n<pre>{}</pre>\n</body>\n'.format(tb[0],''.join(tb[1]))
    sys.stderr.write(ausgabe)
    sys.stderr.flush()
    print(ausgabe)
    exit(1)


'''
________________________________________________________________________
 ti_lib.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_lib.py kapselt u.a. die Zugriffe auf die Konfigurationsdatenbank.
 Die Zugriffe werden von allen Modulen an ti.py gerichtet, welches sie 
 durchreicht und dann z.B. in int-werte umwandelt.
 
 ti_lib.py kann auch von der Kommandozeile/ Shellskripten aufgerufen werden.

 Es werde bewusst keine anderen ti-Module außer config_db aufgerufen!
________________________________________________________________________
'''

class S_any_vplan():
    def __init__(self,epoche,ziel,phase,spalten,url,dauer,weite=0,qf=False):
        self.epoche=int(epoche)
        self.ziel=ziel;         self.phase=phase
        self.spalten=spalten;   self.url=url
        self.dauer=dauer;       self.weite=weite
        self.qf=qf
   


def isodatum_heute_plus_delta(delta):
    _timestamp_=time.localtime(time.time())
    return ts_2_isodatum(ts_verschiebe(_timestamp_,delta))
    
def epoche_2_isodatum(e):
    return ts_2_isodatum(time.localtime(e))
    
def ts_2_isodatum(ts):
    return time.strftime('%Y-%m-%d',ts)

def ts_verschiebe(ts,delta_tage):
    return time.localtime(time.mktime((ts.tm_year,ts.tm_mon,ts.tm_mday+delta_tage,0,0,0,0,0,-1)))

def ist_isodatum(datum):
    if  len(datum)==10 and \
        datum[:2]=='20' and  \
        datum[4]=='-' and  \
        datum[7]=='-' and  \
        datum[2:4].isdigit() and \
        datum[5:7].isdigit() and \
        datum[8:10].isdigit() and \
        int(datum[5:7])<13 and \
        int(datum[8:10])<32:
            return True
    return False
    

def strg2int(value,default=0):
    try:
        return int(ast.literal_eval(value))
    except:
        return default
        
def strg2bool(value,default=False):
    if len(value)==0:
        return default
    try:
        return (int(ast.literal_eval(value)) !=0)
    except:
        if value[0] in "YyJj1Tt": 
            return True
        if value[0] in "Nn0Ff": 
            return False
    return default
    
    

## basepath: Ort des cgi-Verzeichnisses, Rest basiert darauf
def get_ti_basepath():
    return __ti_basepath__
def get_ti_cgibinpath(): # nach Definition...
    return __ti_basepath__+'bin/'
def get_ti_datapath(parameter=''):
    return __ti_datapath__+parameter
def get_ti_dynpath(parameter=''):
    return __ti_dynpath__+parameter
    

def prt(*args):
    for data in args:
        data8=str(data).encode('utf-8')
        try:
            sys.stdout.buffer.write(data8)
        except:
            sys.stdout.write(data8)
        
def get_now(): # liefert unix-epoche
    return time.time() #sec since epoche
    
#def get_hour_min():
#    return (_timestamp_.tm_hour,_timestamp_.tm_min)
    
def get_akt_zeit():
    _timestamp_=time.localtime(time.time())
    return _timestamp_.tm_hour*100+_timestamp_.tm_min
    
def beschreibe_exception(e):
    #t='Fehlermeldung: '+str(e)+'\n\n'
    #t+=traceback.format_exc()
    #exc_type, exc_value, exc_traceback = sys.exc_info()
    #formatted_lines = traceback.format_exc().splitlines()
    #t+='\n'.join(formatted_lines)
    return '{}: {}\n\nTraceback:\n{}'.format(_('Fehlermeldung'),str(e),traceback.format_exc())
    
def reset_log():
    log('\n'+'#'*73+'\n# '+_('Dieses Logfile wird hiermit abgeschlossen, umbenannt und neu angelegt')+' #\n'+'#'*73)
    try:
        os.remove(__ti_datapath__+'log/'+__logfile__+'old')
    except:
        pass
    os.rename(    __ti_datapath__+'log/'+__logfile__, \
                  __ti_datapath__+'log/'+__logfile__+'.old')
    log(_('Neues Logfile nach Reset'))

def log(*args):
    items=[]
    for i in args:
        items.append(str(i))
    try:
        with codecs.open(__ti_datapath__+'log/'+__logfile__, mode='a', encoding='utf-8') as fout:
                fout.write(time.strftime('%d %b %Y %H:%M:%S',time.localtime())+': '+' '.join(items)+'\n')
                fout.close()
    except Exception as e:
        panic("ti_lib failed on writing logfile",e)

def get_akt_hour():
    _now_=time.time() #sec since epoche
    return time.localtime(_now_).tm_hour

try:
    ###
    # START
    ##
    #### quasi Konstanten, da nur bei neuem Programmstart mit anderen Werten möglich
    ## Zuerst absoluten Pfad die tabula.info-Installation feststellen
    # basepath sollte das Verzeichnis, in dem cgi-bin liegt, wiedergeben, z.B. /opt/tabula.info/tabula2.
    __ti_basepath__= os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+"/"
    __ti_datapath__=os.path.abspath(__ti_basepath__+'../local/data')+"/"
    __ti_dynpath__=os.path.abspath(__ti_basepath__+'../local/www/dyn')+"/"
    __username__=pwd.getpwuid(os.getuid())[0]
    if __username__=='www-data':
        __logfile__='background.log'
    else:
        __logfile__=__username__+'.log'
                    
    thelocale="#unknown"
    try:
        with codecs.open(__ti_datapath__+'locale.conf','r','utf-8') as localedatei:
            while thelocale and not thelocale[0].isalpha():
                thelocale=localedatei.readline().strip()
        if thelocale:
            translate=gettext.translation('tabula',os.path.abspath(__ti_basepath__+'locales/'),languages=[thelocale],fallback=False)
            sys.stderr.write('\nti_lib.py: locale set to '+thelocale+'\n')
            sys.stderr.flush()
        else:
            translate=gettext.translation('tabula',os.path.abspath(__ti_basepath__+'locales/'),languages=['de_DE'],fallback=True) # fallback ist das relevante...
    except Exception as e:
        sys.stderr.write('\nti_lib.py: Problems with locale '+thelocale+':\n'+str(e)+'\n')
        sys.stderr.flush()
        translate=gettext.translation('tabula',os.path.abspath(__ti_basepath__+'locales/'),languages=['de_DE'],fallback=True) # fallback ist das relevante...
    translate.install()

    for pfad in ['upload','work']:
        if not os.path.exists(__ti_datapath__+pfad):
            os.makedirs(__ti_datapath__+pfad)

except Exception as e:
    panic("ti_lib failed initializing",e)

