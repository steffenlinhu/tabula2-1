// dieser Code sorgt fuer ein periodisches Scrollen einer HTML-Datei
// Kontext: s_html.py in tabula.info
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)
// Funktion: Wartet 3s, scrollt dann alle 23ms um 1 PixelZeile bis ...
//           Seite am Ende -> Position veraendert sich nicht mehr -> 
//           Scrollen abschalten, nach 3s Neustart am Seitenanfang
// Wird in der Aufrufadresse ein Parameter RasPi uebergeben
//          ( z.B. bla.html?RasPi=True ), so wird Zeitintervall und Zeilenzahl
//          um RasPiDelta vergrößert, um der Geschwindigkeit der RasPi gerecht zu werden.
// Eine HTML-Seite muss dieses Script im head einbinden mit :
//          <script src="scroll.js" type="text/javascript"></script>
// und aufrufen ueber:
//          <body onload="scrollstart()" >

var linetime=31;            // ms für eine Pixelzeile - die Scrollgeschwindigkeit!
var quicktime=16;           // erhöhte Scrollgeschwindigkeit

var d_s_handle;
var vorhertop=-1;
var nachhertop=-1;
var counter=1;

var delta=1;        // um wie viele Pixelzeilen soll auf einmal gescrollt werden: i.d.R 1 bis 2
var RasPiDelta=7;   // -"- aber für langsame Rechner, i.d.R zwischen 5 und 10
var universal_scroll_top; // enthält bald eine Funktion, welche auf allen Browsern scroll_top ermittelt
var flagPerTimeout=false; // flase: Aufruf mit setIntervall, true: Aufruf mit setTimeout

// Wird 1x z.B. aus dem Body-Tag aufgerufen
function scrollstart() {
    console.log('scroll.js wird vorbereitet');
	if (window.location.search.substring(1).indexOf("RasPi")>=0) {
		delta=RasPiDelta; 
		console.log("RasPi -> slow... -> alle",delta*linetime,"ms um ",delta," Pixelzeilen scrollen");}
	if (window.location.search.substring(1).indexOf("QuickScroll")>=0) {
		linetime=quicktime; 
		console.log("QuickScroll:",delta*linetime,"ms um ",delta," Pixelzeilen scrollen");}
	if (window.location.search.substring(1).indexOf("PerTimeout")>=0) {
		flagPerTimeout=true; 
		console.log("JS-Scrollen wird per timeout erledigt");}
	window.setTimeout(function(){ scrollinit(); },3000);
	window.scrollTo(0,0);
}
	
// Startet das eigentliche Scrollen als regelmaessigen Funktionsaufruf, 
// wird 1x pro Gesamtscrollen der HTML-Seite aufgerufen
function scrollinit() {
	if (flagPerTimeout==true) {
        window.setTimeout("do_scroll()", delta*linetime)
    }
	else {
        d_s_handle=window.setInterval( function(){ do_scroll() },linetime*delta);
    }
}
	
// Wird immer wieder aufgerufen (konfiguriert in scrollinit() )
function do_scroll() {
	window.scrollBy(0,delta);
	nachhertop=universal_scroll_top();
	if ( vorhertop==nachhertop) {
		if (flagPerTimeout==false) {
            window.clearInterval(d_s_handle)
        }
		window.setTimeout("scrollstart()",5000);
	}
	else {
		vorhertop=nachhertop;
        if (flagPerTimeout==true) {
            window.setTimeout("do_scroll()", delta*linetime)
        }
	}
}

// Vorbereitung:
// Anstatt in der Funktion immer wieder den Browser zu checken, 
// wird das einmal gemacht und eine passende anonyme Funktion erzeugt...
// (eigentliche Abfrage inspiriert von http://stackoverflow.com/questions/871399)
if(typeof pageYOffset!= 'undefined') {
	universal_scroll_top= function() { return pageYOffset; }  }
else {
	if (document.documentElement.clientHeight) {
		universal_scroll_top= function() { return document.documentElement.scrollTop; } 
	}
	else {
		universal_scroll_top= function() { return document.body.scrollTop; }
	}
}

