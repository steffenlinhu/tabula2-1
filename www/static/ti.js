// dieser Code erledigt Kleinigkeiten
// Kontext: frame.py in tabula.info
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)


var d_s_handle;
var ti_time_element;
var counter=50;
var timeout=1000;
var counterdelta=1;       

// Wird 1x z.B. aus dem Body-Tag aufgerufen
function ti_start() {
	if (window.location.search.substring(1).indexOf("RasPi")>=0) {
		timeout=2000; 
        counterdelta=2;
		console.log("RasPi -> slow... -> alle 2 Sekunden Zeit aktualisieren");}
    ti_time_element=document.getElementById("ti_time");
    counter=parseInt(ti_time_element.innerHTML)
	ti_time();
    d_s_handle=window.setInterval( function(){ ti_time() },timeout);
    startelementscrolling();
}
function startelementscrolling() {
    var allelements = document.getElementsByClassName('scrollcontent');
    console.log(allelements,"!");
    for (let oneelement of allelements) {
        scrollelement(oneelement);
    }
}
// Wird immer wieder aufgerufen 
function ti_time() {
    ti_time_element.innerHTML=" "+counter+"s";
    if (counter>=counterdelta) {
        counter-=counterdelta;
    }
    else {
        counter=0;
    }
}

// Nun: Scrollen eines div-Inhalts:
// scrollelement(document.getElementById('scrollcontent'));
    
function scrollelement(element) {
    if (element!=undefined) {
        var lasttop = element.scrollTop;
        const animateScroll = function(){
            var newtop=lasttop+1;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop===lasttop) {
                //console.log(lasttop);
                setTimeout(animateScroll, 20);
            }
            else {
                //console.log("ganz unten");
                setTimeout(animateReScroll, 5000);
            };
        };
        const animateReScroll = function(){
            var newtop=lasttop-5;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop>0) {
                //console.log(lasttop);
                setTimeout(animateReScroll, 20);
            }
            else {
                //console.log("ganz oben");
                setTimeout(animateScroll, 5000);
            };
        };
        setTimeout(animateScroll, 5000);
    }
}

